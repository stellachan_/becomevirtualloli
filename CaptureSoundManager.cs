﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Audio;

public class CaptureSoundManager : MonoBehaviour
{
    public GameObject objectTarget = null;
    public Animator animator;
    protected AudioClip textureWebCam = null;
    bool m_Play = false;
    bool m_ToggleChange = true;
    private AudioSource _audio;
    public AudioMixer _MasterMixer;
   
    // Start is called before the first frame update
    void Start()
    {
        UnityEngine.Debug.Log("Sound Manager Start~");
        foreach (var device in Microphone.devices)
        {
            UnityEngine.Debug.Log("Name: " + device);
            if (device == "WET (VT-4)" || device == "마이크 (VIVE Pro Mutimedia Audio)")
            {

                AudioSource aud = objectTarget.AddComponent<AudioSource>();
                _audio = aud;
                aud.clip = Microphone.Start(device, true, 1, 22050);
                aud.outputAudioMixerGroup = _MasterMixer.FindMatchingGroups("VoiceMute")[0];
                aud.loop = true;
                //while (!(Microphone.GetPosition(device) > 0)) { }
                aud.Play();
                m_Play = true;
            }
        
        }
        
        
        /*
        int selectedMicIndex = -1;
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i]);
            if (devices[i] == "AVerMedia GC550 Video Capture")
            {
                Debug.Log("Find!!!!");
                selectedMicIndex = i;
            }
        }
        */

    }

    void Update()
    {
        float loudness = GetAveragedVolume() * 100;
        //UnityEngine.Debug.Log("vol: " + loudness);
        animator.SetFloat("Voice", loudness);
    }

    float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        _audio.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }


}
