﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetTargetIK : MonoBehaviour
{
    [SerializeField]
    float zOffsetPos = 2f;

    Vector3 headRotation = new Vector3(0f, 180f, 0f);

    [SerializeField]
    Vector3 handsRotation = new Vector3(0f, 180f, 0f);

    [Tooltip("transformsToFollow and transformsToOffset arrays need to match. Swap Left/Right for mirror")]
    [SerializeField]
    Transform[] transformsToFollow;

    [SerializeField]
    Transform[] transformsToOffset;

    private void Awake()
    {
        if (transformsToOffset.Length == 0)
        {
            transformsToOffset = this.GetComponentsInChildren<Transform>();
        }
        if (transformsToOffset.Length != transformsToFollow.Length)
        {
            Debug.LogError("transformsToFollow and transformsToOffset arrays don't match");
        }

    }


    private void Update()
    {

        for (int i = 0; i < transformsToOffset.Length; i++)
        {

            if (i == 0)
            { // HEAD

                //v1 Mirrors x-axis only
                /* Quaternion offsetRotation = transformsToFollow[i].rotation * Quaternion.Euler(headRotation);
                Quaternion invertedRotation = new Quaternion (offsetRotation.x, -offsetRotation.y, offsetRotation.z, -offsetRotation.w);
                transformsToOffset[i].rotation = invertedRotation; */

                //v2 Mirrors x-axis and y-axis BUT NOT z
                /*Quaternion headCacheAngle = transformsToFollow[i].rotation;
                Quaternion headInvertedRotation = new Quaternion (-headCacheAngle.x, headCacheAngle.y, -headCacheAngle.z, headCacheAngle.w);
                Quaternion headOffsetRotation = Quaternion.Inverse(Quaternion.Euler(headRotation)) * Quaternion.Inverse(headInvertedRotation);
                transformsToOffset[i].rotation = headOffsetRotation;*/

                // Mirror all, with reverse on z-axis
                Quaternion offsetRotation = Quaternion.Inverse(transformsToFollow[i].rotation) * Quaternion.Euler(headRotation);
                Vector3 targetEuler = offsetRotation.eulerAngles;
                transformsToOffset[i].rotation = Quaternion.Euler(targetEuler.x, targetEuler.y, -targetEuler.z);

            }
            else
            { // HANDS

                // Y:180
                Quaternion cacheAngle = transformsToFollow[i].rotation;
                Quaternion handsInvertedRotation = new Quaternion(-cacheAngle.x, -cacheAngle.y, cacheAngle.z, cacheAngle.w);
                Quaternion handsOffsetRotation = handsInvertedRotation * Quaternion.Inverse(Quaternion.Euler(handsRotation));
                transformsToOffset[i].rotation = handsOffsetRotation;

            }

            transformsToOffset[i].position = transformsToFollow[i].position + new Vector3(0f, 0f, zOffsetPos);

            // ?? Hands z- axis movement is inverted
        }
    }
}