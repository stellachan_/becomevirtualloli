﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine;

public class PunchEffectScript : MonoBehaviour
{
    Transform thistransform;
    float prevVelocity;
    float needEffectVelocity;
    int frameCount;
    int frameCountCycle;
    bool isEffectCoolTime;
    bool RoofTrigger;
    public ParticleSystem StarBust;
    // Start is called before the first frame update
    void Start()
    {
        frameCount = 0;
        RoofTrigger = true;
        frameCountCycle = 5;
        needEffectVelocity = 0.15f;
        isEffectCoolTime = true;
        thistransform = this.GetComponent<Transform>();
        StartCoroutine("PunchEffect");
        UnityEngine.Debug.Log("Punch Effect Check Start!");
    }

    IEnumerator PunchEffect()
    {
        while(RoofTrigger)
        {
            Vector3 vel = thistransform.position;
            float posX = vel.x;
            float posY = vel.y;
            float posZ = vel.z;

            if (posX < 0)
            {
                posX = posX * -1;
            }
            if (posY < 0)
            {
                posY = posY * -1;
            }
            if (posZ < 0)
            {
                posZ = posZ * -1;
            }

            float posTotal = (posX + posY + posZ);

            if (isEffectCoolTime == true)
            {
                
                //UnityEngine.Debug.Log(posTotal);

                float posAbsoluteVal = prevVelocity - posTotal;

                if (posAbsoluteVal < 0)
                {
                    posAbsoluteVal = posAbsoluteVal * -1;
                }

                if (posAbsoluteVal > needEffectVelocity)
                {
                    UnityEngine.Debug.Log("Punch Effect!");
                    StarBust.Play();
                    isEffectCoolTime = false;
                    frameCount = 0;
                }
            }

            if (frameCount == frameCountCycle)
            {
                frameCount = 0;
                isEffectCoolTime = true;
            }
            prevVelocity = posTotal;

            frameCount++;

            yield return new WaitForSeconds(0.025f);
        }
    }

    void OnApplicationQuit()
    {
        RoofTrigger = false;
        StopCoroutine("PunchEffect");
    }
}
