﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripAndWheelRotate : MonoBehaviour
{
    public Transform leftHand;
    public Transform uppo;
    public Transform controlWheel;
    private float closeDis = 5.0f;
    private bool isSetCenter = false;
    private bool isfliped = false;
    private Quaternion centerPosition;
    private Vector3 OriRotate;
    private float damping = 2f;
    private Vector3 prevPosition;
    // Start is called before the first frame update
    void Start()
    {
        OriRotate = controlWheel.eulerAngles;

    }

    // Update is called once per frame
    void Update()
    {
        if (leftHand)
        {
            Vector3 leftoffset = leftHand.position - transform.position;
            float leftsqrLen = leftoffset.sqrMagnitude;
            //Vector3 rightoffset = rightHand.position - transform.position;
            //float rightsqrLen = rightoffset.sqrMagnitude;

            if (leftsqrLen < 0.05f)
            {
                Vector3 targetPostition = new Vector3(leftHand.position.x, leftHand.position.y,  this.transform.position.z);
                this.transform.LookAt(targetPostition);
                
                Vector3 myRotation = this.transform.rotation.ToEulerAngles();
                if (myRotation.y < 0)
                {
                    //this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, this.transform.localScale.z);
                    isfliped = false;
                }
                else if(myRotation.y > 0)
                {
                    //this.transform.localRotation = new Vector3(this.transform.localRotation.x, this.transform.localRotation.y * -1, this.transform.localRotation.z);
                    //this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, this.transform.localScale.z);
                    this.transform.Rotate(new Vector3(this.transform.localRotation.x+180f, 180f, this.transform.localRotation.z));
                    isfliped = true;
                }

                if (isSetCenter == false)
                {
                    isSetCenter = true;
                    controlWheel.eulerAngles = OriRotate;
                    UnityEngine.Debug.Log("Set Up Foward : " + OriRotate, this);
                }

            }
            else
            {
                if(isSetCenter = true)
                {
                    isSetCenter = false;
                    OriRotate = controlWheel.eulerAngles;
                     UnityEngine.Debug.Log("Set Up Foward : "+ OriRotate, this);
                }
                UnityEngine.Debug.Log("Grap off", this);
            }
        }
    }
}
