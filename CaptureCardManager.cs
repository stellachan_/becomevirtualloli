﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatSuite;
using System.Threading.Tasks;
using NatSuite.Devices;
using static NatSuite.Devices.MediaDeviceQuery;

public class CaptureCardManager : MonoBehaviour
{
    public GameObject objectTarget = null;
    public GameObject objectTarget2 = null;
    //protected WebCamTexture textureWebCam = null;
    public Camera charCamera;
    public Camera stageCamera;
    public Camera CloseUpCamera;
    public Camera Camera1;
    public Camera Camera2;
    public Camera Camera3;


    //public GameObject Head = null;
    // Start is called before the first frame update
    MediaDeviceQuery query;
    async void Start()
    {
        Debug.Log("displays connected: " + Display.displays.Length);
        // Display.displays[0] is the primary, default display and is always ON.
        // Check if additional displays are available and activate each.
        if (Display.displays.Length > 1)
            Display.displays[1].Activate();
        if (Display.displays.Length > 2)
            Display.displays[2].Activate();
        if (Display.displays.Length > 3)
            Display.displays[3].Activate();

        charCamera.enabled = true;
        stageCamera.enabled = true;
        CloseUpCamera.enabled = false;
        Camera1.enabled = false;
        Camera2.enabled = false;
        Camera3.enabled = false;



        //Debug.Log("CAPTURE SOURCE FIND START");
        query = new MediaDeviceQuery(Criteria.GenericCameraDevice);
        UnityEngine.Debug.Log(query);
        // Start camera preview
        var device = query.currentDevice as ICameraDevice;
        UnityEngine.Debug.Log(query.currentDevice);
        if(query.currentDevice.ToString() == "AVerMedia GC550 Video Capture")
        {
            UnityEngine.Debug.Log("Not CaptureBoard");
            return;
        }
        var previewTexture = await device.StartRunning();
        UnityEngine.Debug.Log($"Started camera preview with resolution {previewTexture.width}x{previewTexture.height}");

        // Display preview texture
        Renderer render = objectTarget.GetComponent<Renderer>();
        Renderer render2 = objectTarget2.GetComponent<Renderer>();

        render.material.mainTexture = previewTexture;
        render2.material.mainTexture = previewTexture;
        
       

        //previewPanel.texture = previewTexture;
        //previewAspectFitter.aspectRatio = previewTexture.width / (float)previewTexture.height;

        /*
        WebCamDevice[] devices = WebCamTexture.devices;
        int selectedCameraIndex = -1;
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
            if (devices[i].name == "AVerMedia GC550 Video Capture")
            {
                //Debug.Log("Find!!!!");
                selectedCameraIndex = i;
            }
        }
        // WebCamTexture 생성
        if (selectedCameraIndex >= 0)
        {
            // 선택된 카메라에 대한 새로운 WebCamTexture를 생성
            textureWebCam = new WebCamTexture(devices[selectedCameraIndex].name,640,360);


            // 원하는 FPS를 설정
            if (textureWebCam != null)
            {
                textureWebCam.requestedFPS = 60;
            }
        }
        */

        // objectTarget으로 카메라가 표시되도록 설정
        /*
        if (textureWebCam != null)
        {
            // objectTarget에 포함된 Renderer
            //Renderer render = objectTarget.GetComponent<Renderer>();
            Renderer render2 = objectTarget2.GetComponent<Renderer>();

            // 해당 Renderer의 mainTexture를 WebCamTexture로 설정
            //render.material.mainTexture = textureWebCam;
            //render2.material.mainTexture = textureWebCam;
        }

        textureWebCam.Play();
        */

    }

    // Update is called once per frame
    //void Update()
    //{
     //   if(CloseUpCamera.enabled)
     //   {

   //     }
        
   // }
   /*
    void OnDestroy()
    {
        // WebCamTexture 리소스 반환
        if (textureWebCam != null)
        {
            textureWebCam.Stop();
            WebCamTexture.Destroy(textureWebCam);
            textureWebCam = null;
        }
    }

    // Play 버튼이 눌렸을 때
    // 주의! 유니티 Inspector에서 버튼 연결 필요
    public void OnPlayButtonClick()
    {
        // 카메라 구동 시작
        if (textureWebCam != null)
        {
            textureWebCam.Play();
        }
    }

    // Stop 버튼이 눌렸을 때
    // 주의! 유니티 Inspector에서 버튼 연결 필요
    public void OnStopButtonClick()
    {
        // 카메라 구동 정지
        if (textureWebCam != null)
        {
            textureWebCam.Stop();
        }
    }
   */


}
