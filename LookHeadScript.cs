﻿using UnityEngine;

public class LookHeadScript : MonoBehaviour
{
    public Transform Head;
    private Camera mycam;
    // Start is called before the first frame update
    void Start()
    {
        this.mycam = GetComponent<Camera>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if(mycam.enabled)
        {
            transform.LookAt(Head);
        }
    }
}
